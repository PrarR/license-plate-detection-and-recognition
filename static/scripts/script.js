
$(".imageButton").click(function()
{
    $('*').css("cursor", "progress");
    postdata(this);
});

function postdata(link) {
$('#response').html('<div class="spinner-border text-success" style="width: 3rem; height: 3rem;" role="status"></div>');
    value = $(link).attr('value')
$.ajax({
        type: "POST",
        url: '/img',
        data: value,
        success: function(data) {
            $('#response').html(data)
            $('*').css("cursor", "default");
        },
        error: function() {
            alert('Error occured');
            $('*').css("cursor", "default");
        }
    });
}
