# License Plate Detection and Recognition
Broadening horizons and gaining skills in programming in Python language, as well as using OpenCV and NumPy libraries to implement the course "Image processing in multimedia systems - The project". In addition, the aim of the project will be to perform a system for detection and recognition of license plates and analysis of images using mathematical models, which can be used, for example, in a shopping mall.

# Design assumptions:
- The algorithm correctly interprets the car license plates valid in the European Union
- The input images should be well illuminated for correct detection and recognition.
- The license plate should be placed at a suitable angle, which helps the algorithm to recognize the characters from the license plate (preferred position in relation to the image recorder - perpendicular to the plate)
- For proper detection, use a basic plate (white background/black border and text)
- The plate should be readable.
- Image resolution should not be less than 640x360px.

# Team
- Piotr Kupczyk - Lead Python Developer
- Jacek Urniaż - Python Developer
- Filip Wojakiewicz - Python Developer
